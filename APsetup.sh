#! /bin/bash

sudo pkill -f prod
sudo cp /home/pi/ExoLab/network_setup/* /etc/
sudo service networking restart
sudo service hostapd stop
sudo /etc/init.d/dnsmasq restart
sudo hostapd -B /etc/hostapd/hostapd.conf
sleep 5
sudo python /home/pi/ExoLab/flask/run.py
