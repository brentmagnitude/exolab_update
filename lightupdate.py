# V33 10/21/18 LK 
#     upload one image at a time from data/picbuf  
import time
import math
import urllib2
import json
from datetime import datetime
import pigpio
from PIL import Image
from shutil import copyfile
import subprocess
import os
import requests
from neopixel import *

os.chdir("/home/pi/ExoLab") # Make sure we are in program home directory, even if started outside

# LED strip configuration:
LED_COUNT      = 64      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
#LED_STRIP = ws.WS2811_STRIP_GRB
pic_period=3600 #should be 3600!!!  for testing I usually temp set to 5 mins (300 seconds)

startHour=7;	# Turn lights on at 7 local time


pi=pigpio.pi()
buttonPin=16
pi.set_mode(buttonPin, pigpio.INPUT) #Set button on device to input

sawtooth=range(4)*8
half=[int(e+8*math.floor((i/4))) for i, e in enumerate(sawtooth)] #64 LEDs exist. This comprehension finds those on the left side of the device.
ohalf=[h+4 for h in half] #This finds the rest

hueDefs={'green':[255,0,0],
               'blue':[0,0,255],
               'red':[0,255,0],
               'white':[255,255,255],
               'magenta':[0,255,255]} #match the colors from the on-device JSON/online json to GRB values for the light library

credfile=open("/home/pi/ExoLab/cred")
cred=credfile.read()[:24]
credtuple=tuple(cred.split(':')) #Get credentials to access the correct light value from the platform.
warningTime=10 #blinks to blink before waiting 1 minute before snapping a photo.

#Ingredients to assemble json to upload images
json_open='\{\\"data\\":['
ts_open='\{\\"timestamp\\":\\"'
ts_close='\\",\\"filename\\":\\"image.jpg\\"\}'
json_close='],\\"upload_file\\":[\\"filename\\"]\} '
picsfolder = "data/pics/"
picbuffolder = "data/picbuf/"


def display(strip,rightColor,leftColor): #Display light colors on left and right sides
    for led in ohalf:
            strip.setPixelColor(led, rightColor)

    for led in half:
            strip.setPixelColor(led, leftColor)
    strip.show()

def settingToValue(settings): #convert json value in hue, photoperiod, and intensity, to a Color
    nowHour = datetime.now().hour
    validHours= range(startHour, startHour+settings['photoperiod']-1)
    intensity=(nowHour in validHours)*settings['intensity']
    hue = hueDefs[settings['color']]
    colorArray= [component*intensity/10 for component in hue]
    color = Color(colorArray[0],colorArray[1],colorArray[2]) 
    return color

def arrayize(): #get the setting from the platform using the credential, if possible.
    try:
        r=requests.get('https://classroom.magnitude.io/api/experiments/config/', auth=credtuple)
        settings=r.json()['result']
        assert 'mode' in settings
    except: #If not, use the default lighting 
        defaultLight=open("/home/pi/ExoLab/color.json")
        settings=json.loads(defaultLight.read())
    return settings

def setLight(strip):	#set colors according to settings from platform and send it to display to start lights.
    settings=arrayize()
    if settings['mode']=='dual':
            leftColor = settingToValue(settings['leftSettings'])
            rightColor = settingToValue(settings['rightSettings'])
    elif settings['mode']=='single':
            leftColor = settingToValue(settings['settings'])
            rightColor = settingToValue(settings['settings'])
    print str(leftColor) + " " + str(rightColor)
    display(strip, leftColor, rightColor)   

def flash(strip):		#Show only back row of lights as flash for camera
    for dark in range(8):
            strip.setPixelColor(dark,Color(255,255,255))
    for flash in range(8,64):
            strip.setPixelColor(flash,Color(0,0,0))
    strip.show()

def clear(strip):	#make dark
    for dark in range(64):
            strip.setPixelColor(dark,Color(50,50,50))
    strip.show()

def bright(strip):	#full white
    for dark in range(64):
            strip.setPixelColor(dark,Color(0,0,0))
    strip.show()

def imageupload():
    
    iso=datetime.utcnow().isoformat()+"000Z"
    print("Taking picture!")	#generate names for image to be taken
    picname = picsfolder+iso + '.jpg'
    picbufname = picbuffolder+iso + '.jpg' 
    subprocess.call(["raspistill", "-co", "10", "-sa", "5","-sh", "15",  "-q", "15","-rot", "180", "-n", "-o", picname ]) #take image, modify color, saturation, sharpness a little
    img=Image.open(picname) #Rotate image after taking to correct for orientation of camera. If this is done before, the image is cropped.
    img=img.rotate(90)
    img.save(picname)
    
    copyname="./"+picname
    copyfile(copyname,picbufname)#Copy image to buffer folder as well

    bufd_imgs=os.listdir(picbuffolder) #Get all images from buffer folder and send each one individually using json structure / api
    # V33 send each file with a separate API call
    print "Files to process:", len(bufd_imgs)
    for fill in bufd_imgs : 
        tslist = [ts_open+fill[:-4]+ts_close]
        tsstr=','.join(tslist)
        targo=picbuffolder+fill
        print "targo=",targo
        compo=json_open+tsstr+json_close    
        uploadCommand='bash imageupload.sh '+compo + cred + ' ' + targo #Use shell script to upload one image using json.
        print uploadCommand
        pipe=subprocess.Popen(uploadCommand, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        out,err=pipe.communicate()
        result=out.decode()
        if "amazonaws" in result: #If successful upload, we expect amazonaws to respond, and we can clear the buffer.            
            print "Success - we'll remove:",targo
            os.remove(targo)
        print result
        log=open("/home/pi/log.log",'a')
        log.write(result)
        log.close()

    
# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    # Intialize the library (must be called once before other functions).
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
    strip.begin()
    setLight(strip) #This command finds and sets the appropriate lighting to start with
    start=0
    while(True):
            if pi.wait_for_edge(buttonPin,pigpio.RISING_EDGE): #If the button is pressed, find and set the lighting immediately.
                    time.sleep(.1)
                    setLight(strip)
            elapsed = time.time() - start
            if elapsed > pic_period:	#If more than an hour has passed, start blinking, then take and upload a pic
                for i in range(warningTime):
                    clear(strip)
                    time.sleep(.25)
                    bright(strip)
                    time.sleep(.25)
                flash(strip)    
                time.sleep(60)
                imageupload()
                start=time.time()
                setLight(strip)	#after uplaod pic, return to normal lighting
