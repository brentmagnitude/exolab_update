#!/bin/bash
# DEV/PRODUCTION SWITCHER
# 9/16/18 - Liam Kennedy
# This script allows you to quickly change the source of the ExoLab updates
# between the Dev and Production repos
#


# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37

#    .---------- constant part!
#    vvvv vvvv-- the code from above
RED='\033[0;31m'
GREEN='\033[0;32m' 
YELLOW='\033[1;33m'
NC='\033[0m' # No Color
DONE=0
echo
echo -e "${YELLOW}DEV/PRODUCTION SWITCHER${NC}"
echo "Current updates will be sourced from:"
cat update.sh | grep 'git clone'
if grep --quiet kareem update.sh ; then
   echo -e "  That is the ${GREEN}PRODUCTION${NC} exolab_update repo"
   CURRENT="prod"
else
   echo -e "  That is the ${RED}DEVELOPMENT${NC} exolab_update repo"
   CURRENT="dev"
fi 

if [ "$1" == "dev"  ]
then 
   if [ "$CURRENT" == "prod" ]
   then
       echo -e "SWITCHING to ${RED}DEV MODE${NC}"
       sed -i -- 's#kareem_magnitude/#issabove/#' update.sh updateURL.json
       DONE=1
   else 
       echo -e "${NC}ALREADY in DEV MODE${NC}"
   fi
elif [ "$1" == "prod" ] 
then 
   if [ "$CURRENT" == "dev" ]
   then 
       echo -e "SWITCHING to ${GREEN}PROD${NC} MODE"
       sed -i -- 's#issabove/#kareem_magnitude/#g' update.sh updateURL.json
       DONE=1
   else 
       echo -e "${YELLOW}ALREADY in PROD MODE${NC}"
   fi
else 
   echo "USAGE "
   echo "  ./switchdev.sh dev"
   echo "or"
   echo " ./switchdev.sh prod"
   echo 
fi

# show what it's now set to 
# TODO: what if it doesn't work?  
if [ $DONE == 1 ];
then
    echo
    echo "Updates will now be sourced from:"
    
    if grep --quiet kareem update.sh ; then
       echo -e "   The ${GREEN}PRODUCTION${NC} exolab_update repo"
       FINAL="prod"
    else
       echo -e "   The ${RED}DEVELOPMENT${NC} exolab_update repo"
       FINAL="dev"
    fi 
    echo 
    echo "update.sh:"
    cat update.sh | grep 'git clone'
    echo
    echo "updateURL.json:"
    cat updateURL.json | grep 'softwareURL'
    echo
fi
    
